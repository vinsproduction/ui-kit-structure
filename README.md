### UI-kit npm structure example

Process:
* files (typescript) from src folder compiled to public folder
* static files (css, fonts) from src folder copy to public folder

Demo, like the production project, uses public folder with js or src with typescripts

***

Run demo on http://localhost:9999
```
npm run dev
```
