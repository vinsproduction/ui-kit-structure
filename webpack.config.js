/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const Dotenv = require('dotenv-webpack');

const DEV_SERVER_PORT = 9999;

const SRC = path.resolve(__dirname, './demo/src');
const DEST = path.resolve(__dirname, './demo/dest');

if (!fs.existsSync(DEST)) { fs.mkdirSync(DEST); }

module.exports = env => {

    const ENV = env || {};

    const DEV_MODE = ENV.development;
    const PROD_MODE = ENV.production;
    const MODE = DEV_MODE ? 'development' : 'production';

    const ENTRY = path.resolve(SRC, 'test.js');

    console.log(JSON.stringify({ ENV, MODE, DEV_SERVER_PORT, ENTRY, SRC, DEST }, null, 2));

    return {

        mode: MODE,

        devtool: DEV_MODE ? 'source-map' : 'cheap-module-source-map',

        entry: {
            app: ENTRY,
        },

        output: {
            path: DEST,
            publicPath: '/',

            filename: PROD_MODE ? 'bundles/[name]/[name].[contenthash].min.js' : 'bundles/[name]/[name].js',
            chunkFilename: PROD_MODE ? 'chunks/[name]/[name].[contenthash].min.js' : 'chunks/[name]/[name].js',
        },

        devServer: {
            port: DEV_SERVER_PORT,
            contentBase: DEST,
            historyApiFallback: {
                rewrites: [
                    { from: /^\/$/, to: '/index.html' },
                ],

            },

            stats: {
                children: false,
                chunks: false,
                chunkModules: false,
                modules: false,
                reasons: false,
                entrypoints: true,
            },
        },

        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                            options: {
                                hmr: DEV_MODE,
                            },
                        },
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: DEV_MODE,
                            },
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true,
                            },
                        },
                    ]
                },
                {
                    test: /\.(otf|ttf|eot|woff|woff2|svg)$/,
                    exclude: /node_modules/,
                    use: 'file-loader?hash=sha512&context=src&name=[path][name].[ext]',
                },
            ]
        },

        resolve: {
            extensions: ['.js', '.jsx', '.ts', '.tsx'],
        },

        optimization: {

            splitChunks: {
                cacheGroups: {
                    vendor: {
                        name: 'vendor',
                        chunks: 'all',
                        priority: 20,
                        test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
                    },
                    common: {
                        name: 'common',
                        minChunks: 2,
                        chunks: 'all',
                        priority: 10,
                        reuseExistingChunk: true,
                        enforce: true
                    }
                }
            },

            minimizer: [

                new OptimizeCSSAssetsPlugin(),

                new TerserPlugin({
                    sourceMap: true,
                    cache: false,
                    parallel: true,
                    extractComments: true,
                    terserOptions: {
                        mangle: true,
                        parse: {},
                        compress: {
                            warnings: true,
                            drop_console: !ENV.console,
                        },
                    },
                }),
            ],
        },

        plugins: [

            new Dotenv(),

            new CleanWebpackPlugin(),

            new webpack.DefinePlugin({
                '__WEBPACK__': JSON.stringify({
                    env: ENV,
                })
            }),

            new MiniCssExtractPlugin({
                filename: PROD_MODE ? 'bundles/[name]/[name].[contenthash].min.css' : 'bundles/[name]/[name].css',
                chunkFilename: PROD_MODE ? 'chunks/[id]/[id].[contenthash].min.css' : 'chunks/[id]/[id].css',
            }),

            new HtmlWebpackPlugin({
                inject: false,
                hash: true,
                template: path.resolve(SRC, 'index.ejs'),
                filename: './index.html',
                chunksSortMode: 'manual',
                chunks: ['vendor', 'app'],
                title: 'UI-kit 2.0',
            })
        ],

        node: {
            __filename: true,
            __dirname: true,
            console: true,
            fs: 'empty',
            net: 'empty',
            tls: 'empty'
        },

        performance: {
            hints: PROD_MODE ? 'warning' : false
        },

        stats: {
            children: false,
            chunks: false,
            chunkModules: false,
            modules: false,
            reasons: false,
        },

    };

};
